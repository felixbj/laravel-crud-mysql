<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use  App\Http\Controllers\BookController; // para estableces los endpoins de este controller
use  App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::resource('books',BookController::class);
    Route::post('books/{book}/genres',[BookController::class,'genres'])->name('books.genres');
    Route::post('books/{book}/author',[BookController::class,'author'])->name('books.author');
});

Route::post('login',[LoginController::class,'signin'])->name('user.login');