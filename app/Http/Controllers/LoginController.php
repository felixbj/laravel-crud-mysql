<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\loginUser;
use App\Http\Controllers\Exception;
use App\Models\User;// para usar el modelo user

class LoginController extends Controller
{
    protected $exception;
    public function __construct(Exception $exception){$this->exception = $exception;}
    /**
     * Registro para iniciar sesión
     * @param  App\Http\Requests\loginUser  $request
     */
    public function signin(loginUser $request){
        $credentials = $request->validated();
        if(!User::validateCredentials($credentials))return $this->exception->Unauthorized();
        $token = User::signin($credentials);
        return response()->json([
            "message" => "Exito al autenticar",
            "token" => $token
        ],201);
    }
}
/**NOTAS
 * // Revoke all tokens...
$user->tokens()->delete();

// Revoke the user's current token...
$request->user()->currentAccessToken()->delete();

// Revoke a specific token...
$user->tokens()->where('id', $id)->delete();

if ($user->tokenCan('server:update')) {
    //
}
 */