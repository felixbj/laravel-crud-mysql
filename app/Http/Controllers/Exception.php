<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Exception extends Controller
{
    public function BadRequestException(){
        return response()->json([
            "message" => "Bad Request"
        ],400);
    }
    public function Unauthorized(){
        return response()->json([
            "message" => "unauthorized"
        ],401);
    }
    public function RecordNotFound(){
        return response()->json([
            "message" => "Record not found"
        ],404);
    }
    public function QueryException($info){
        return response()->json([
            "message" => "Query exception",
            "info" => $info
        ],409);
    }
}
/**
 * ESTADOS HTTP
 * 
 * 2xxExitosos
 * 200 ok
 * 201 creado
 * 204 sincontenido
 * 
 * 3xxRedireccion
 * 301 movidopermanntemete
 * 302|303 busca en esta otra url
 * 307 redireccionado temporalmente
 * 308 redireccionado permanente
 * 
 * 4xxErrorCliente
 * 400 badrequest (error en la peticion)
 * 401 unauthorized (se qe quien eres)
 * 403 Forbidden (no tienes permiso)
 * 404 NotFound (no encontrado)
 * 405 MethodNotAllowend (Metodo no soportado)
 * 409 Conflict
 * 
 * 5xxInternalErrorServer
 * 500 InternalServerError
 * 502 BadGateway
 * 503 ServiceUnavailable
 * 
 * RESPUESTA JSON:API
 * 
 * {
 *  "data":{
 *      "type":"schema/table/model",
 *      "id":"id"
 *      "attributes":{
 *          //atributos de este type
 *      }
 *  }
 * .............
 *  "relationships":{
 *      //atributos de este type
 *      "data":{["type":"type","id":"id",....]},
 *      "link":{
 *          "self":{//link manipular la relation },
 *          "related":{}
 *      },
 *      "meta"{//informacion adicional}
 *  },
 * "links":{//link a si mismo,pagination},
 * "meta":{//info adicional}
 * ......................
 * "errors":[
 * {
 *  "status":402
 *  "title":"Unprocessable Rntity"
 *  "detail":" ... "
 *  "source":{
 *      "pointer":" done esta el error "
 *  }
 * }
 * ]
 * }
 * 
 * QUERY PARAMETERS
 *  INCLUDE... que relatcion incluir en la respuesta
 *  - urlserve/type?include=relationchips1,relationchips2
 *  SORT... ordenar
 *  - urlserve/type?sort=-created_at,title
 *  FIELDS... que atributos especificos
 *  - urlserve/type?fields[type]=title
 *  FILTER....  filtro en la busqueda
 *  - urlserve/type?filter[type]=laravel
 *  PAGE... paginacion
 *  - urlserve/type?pape[size]=10&page[number]=2
 * 
 * ................................. 
 * 
 *  Content-Type:aplication/json
 *  Content-Type:aplication/vnd.api+json
 */
