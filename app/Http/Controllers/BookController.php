<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\book\storeBook; //reques con sus reglas especificas
use App\Http\Requests\book\updateBook; //reques con sus reglas especificas
use App\Http\Requests\book\genreBook; //reques con sus reglas especificas
use App\Http\Requests\book\autorBook; //reques con sus reglas especificas
use App\Http\Controllers\Exception;
use App\Models\Author;
use App\Models\Book; // para usar  modelo Book
use App\Models\Genre; // para usar  modelo Genre

class BookController extends Controller
{
    protected $exception;
    public function __construct(Exception $exception){$this->exception = $exception;}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Book::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request\book\storeBook  $request
     * @return \Illuminate\Http\Response
     */
    public function store(storeBook $request)
    {
        $inputs = $request->validated();
        try {
            $book = Book::create($inputs);
            $book->genres()->sync([1]);// Genero Ninguno por defecto
        } catch (\Illuminate\Database\QueryException $e) {
            return $this->exception->QueryException($e->errorInfo);
        }
        return $book;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::with(['genres','author'])->find($id);
        if(!$book) return $this->exception->RecordNotFound();
        return $book;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request\book\updateBook  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateBook $request, $id)
    {
        $inputs = $request->validated();
        $f = Book::where('id', $id)->get();
        if(!count($f)) return $this->exception->RecordNotFound();
        try {
            $book = Book::where('id', $id)->update($inputs);
        } catch (\Illuminate\Database\QueryException $e) {
            return $this->exception->QueryException($e->errorInfo);
        }
        return $book;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$book = Book::where('id', $id)->get();
        //if(!count($book)) return $this->exception->RecordNotFound();
        $book = Book::find($id);
        if(!$book) return $this->exception->RecordNotFound();
        return Book::where('id', $id)->delete();
    }

    /**
     * Function to Sync genres the book
     * @param  \Illuminate\Http\Request\book\genreBook  $request
     * @param  int  $id
     */
    public function genres(genreBook $request,$id){
        $genreIds = $request->validated()['genres'];
        for ($i=0; $i < sizeof($genreIds); $i++) { 
            $f = Genre::where('id', $genreIds[$i])->get();
            if(!count($f)) return $this->exception->RecordNotFound();
        }
        $book = Book::find($id);
        if(!$book) return $this->exception->RecordNotFound();
        $book->genres()->sync($genreIds);
        return Book::with('genres')->find($id);
    }
    /**
     * Function to associate author the book
     * @param  \Illuminate\Http\Request\book\autorBook  $request
     * @param  int  $id
     */
    public function author(autorBook $request,$id){
        $authorId = $request->validated()['author'];
        $author = Author::find($authorId);
        if(!$author) return $this->exception->RecordNotFound();
        $book = Book::find($id);
        if(!$book) return $this->exception->RecordNotFound();
        $book->author()->associate($author);
        $book->save(); /// importante 
        return Book::with('author')->find($id);
    }
}
