<?php

namespace App\Http\Requests\book;

use Illuminate\Foundation\Http\FormRequest;

class storeBook extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'=> ['required','min:13','max:13'],
            'title'=> ['required','min:2','max:150'],
            'pages'=> ['required','numeric'],
            'edition'=> ['required','numeric'],
            'price'=> ['required','numeric']
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'code.required' => 'A code is required',
            'code.min' => 'The code must be 13 characters long',
            'code.max' => 'The code must be 13 characters long',
            'title.required' => 'A title is required',
            'title.min' => 'The title characters short',
            'title.max' => 'The title characters long',
            'pages.required' => 'A pages is required',
            'pages.numeric' => 'The pages must be a number',
            'edition.required' => 'A edition is required',
            'edition.numeric' => 'The edition must be a number',
            'price.required' => 'A price is required',
            'price.numeric' => 'The price must be a number',
        ];
    }
}
