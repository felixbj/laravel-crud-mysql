<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasFactory,Notifiable,HasApiTokens;
    /**
     * $token=$user->createToken('token-name');return $token->plainTextToken;
     * // Revoke all tokens...
     * $user->tokens()->delete();
     * 
     * // Revoke the user's current token...
     * $request->user()->currentAccessToken()->delete();
     * 
     * // Revoke a specific token...
     * $user->tokens()->where('id', $id)->delete();
     */
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Validar credenciales de ususario
     * @param $credentials
     */
    public static function validateCredentials($credentials){
        $user = User::where('email',$credentials['email'])->first();
        if(!$user || !Hash::check($credentials['password'],$user->password)) return false;
        return true;
    }
    /**
     * Para emitir un token
     * @param $credentials
     * @return String token
     */
    public static function signin($credentials){
        $user = User::where('email',$credentials['email'])->first();
        if(!$user || !Hash::check($credentials['password'],$user->password)) return false;
        return $user->createToken($credentials['device_name'])->plainTextToken;
    }
}
