<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;
    protected $table = "authors";
    protected $fillable = ['code','firstname','lastname'];
    protected $hidden = ['created_at','updated_at','id'];

    public function books()
    {
        return $this->hasMany(Book::class,'author_id','id');
    }
}
