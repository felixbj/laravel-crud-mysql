<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
    protected $table = "books";
    protected $fillable = ['code','title','pages','edition','price'];
    protected $hidden = ['created_at','updated_at','id'];

    public function genres()
    {
        return $this->belongsToMany(Genre::class,'book_genre','book_id','genre_id');
    }
    public function author()
    {
        return $this->belongsTo(Author::class,'author_id','id');// revisar
    }
}
