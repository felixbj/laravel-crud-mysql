<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genres')->insert([
            'code' => '5cabf3e972gf9',
            'name' => 'Ninguna'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3e972db9',
            'name' => 'Policial (o Thriller)'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3e99f730',
            'name' => 'Romántica'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3e9b3f45',
            'name' => 'Aventura'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3e9cd8f4',
            'name' => 'Terror'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3e9e2d2c',
            'name' => 'Ficcion / Realidad'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3e9ecd78',
            'name' => 'Ciencia Ficción'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3ea0e52b',
            'name' => 'Investigación'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3ea2d0ff',
            'name' => 'Biográfica'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3ea448a5',
            'name' => 'Infantil'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3ea5a392',
            'name' => 'Autoayuda'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3ea6d2a6',
            'name' => 'Erótica'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3ea83de3',
            'name' => 'Hogar'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3ea987c7',
            'name' => 'Enciclopedia / Manual'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3eaac854',
            'name' => 'Política'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3eac213e',
            'name' => 'Economía / Marketing'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3ead183c',
            'name' => 'Sociedad'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3eae29d1',
            'name' => 'Deportes'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3eaec83e',
            'name' => 'Viajes / Cultura'
        ]);
        DB::table('genres')->insert([
            'code' => '5cabf3eb06dd8',
            'name' => 'Otros temas / Varios'
        ]);
    }
}
